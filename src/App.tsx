import React, {ChangeEvent, useMemo, useReducer} from 'react'
import Plot from 'react-plotly.js'
import Vector3d from './Vector3d'

const initialVectors = {
  vectors: {
    a: {
      x: '1',
      y: '1',
      z: '-1',
    },
    b: {
      x: '1',
      y: '0',
      z: '0',
    },
  },
  multiplier: '',
}

type State = typeof initialVectors
type VectorName = keyof typeof initialVectors.vectors
type Coordinate = keyof typeof initialVectors.vectors[VectorName]
type Action =
  | {
      type: 'reset'
    }
  | {
      type: 'updateVector'
      payload: {
        vectorName: VectorName
        coordinate: Coordinate
        value: string
      }
    }
  | {
      type: 'updateMultiplier'
      payload: {
        value: string
      }
    }

function reducer(state: State, action: Action) {
  switch (action.type) {
    case 'reset':
      return initialVectors
    case 'updateVector':
      return {
        ...state,
        vectors: {
          ...state.vectors,
          [action.payload.vectorName]: {
            ...state.vectors[action.payload.vectorName],
            [action.payload.coordinate]: action.payload.value,
          },
        },
      }
    case 'updateMultiplier':
      return {
        ...state,
        multiplier: action.payload.value,
      }
    default:
      return initialVectors
  }
}

function App() {
  const [store, dispatch] = useReducer(reducer, initialVectors)
  const handleChange = useMemo(
    () => (
      args:
        | {
            name: VectorName
            coordinate: Coordinate
          }
        | undefined = undefined,
    ) => {
      return (event: ChangeEvent<HTMLInputElement>) => {
        if (!!args) {
          dispatch({
            type: 'updateVector',
            payload: {
              vectorName: args.name,
              coordinate: args.coordinate,
              value: event.target.value,
            },
          })
        } else {
          dispatch({
            type: 'updateMultiplier',
            payload: {
              value: event.target.value,
            },
          })
        }
      }
    },
    [],
  )

  const crossProduct = useMemo(
    () =>
      new Vector3d(store.vectors.a).crossProduct(new Vector3d(store.vectors.b)),
    [store.vectors.a, store.vectors.b],
  )

  const dotProduct = useMemo(
    () =>
      new Vector3d(store.vectors.a).dotProduct(new Vector3d(store.vectors.b)),
    [store.vectors.a, store.vectors.b],
  )
  const scalarProduct = useMemo(
    () =>
      new Vector3d(store.vectors.a).scalarProduct(parseFloat(store.multiplier)),
    [store.vectors.a, store.multiplier],
  )

  const add = useMemo(
    () => new Vector3d(store.vectors.a).add(new Vector3d(store.vectors.b)),
    [store.vectors.a, store.vectors.b],
  )

  return (
    <section
      style={{
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <h2>Vector Operations</h2>

      {(Object.keys(store.vectors) as VectorName[]).map((vectorName) => (
        <div key={vectorName}>
          <span>Vector {vectorName}: </span>
          {(Object.keys(store.vectors[vectorName]) as Coordinate[]).map(
            (coordinate) => (
              <input
                key={coordinate}
                type='number'
                placeholder={coordinate}
                value={store.vectors[vectorName][coordinate]}
                onChange={handleChange({name: vectorName, coordinate})}
              />
            ),
          )}
        </div>
      ))}
      <div>
        <span>Multiplier: </span>
        <input
          type='number'
          placeholder='multiplier'
          value={store.multiplier}
          onChange={handleChange()}
        />
      </div>

      <section>
        <h3>Results:</h3>
        <p>Cross Product: {crossProduct.toString()}</p>
        <p>
          Scalar Product of 'a':{' '}
          {scalarProduct
            ? scalarProduct.toString()
            : 'Please set correct multiplier'}
        </p>
        <p>Add: {add.toString()}</p>
        <p>Dot Product: {dotProduct}</p>
      </section>

      <section
        style={{
          display: 'flex',
          alignItems: 'center',
          padding: '5rem',
          flexDirection: 'column',
        }}
      >
        <Plot
          data={(Object.keys(store.vectors) as Array<VectorName>).reduce(
            (vectors, vectorName) => [
              ...vectors,
              {
                'x': ['0', store.vectors[vectorName].x],
                'y': ['0', store.vectors[vectorName].y],
                'z': ['0', store.vectors[vectorName].z],
                'type': 'scatter3d',
                'mode': 'lines+markers',
                'line.shape': 'hvh',
                'name': `vector ${vectorName}`,
              },
            ],
            [
              {
                'x': ['0', crossProduct.x.toString()],
                'y': ['0', crossProduct.y.toString()],
                'z': ['0', crossProduct.z.toString()],
                'type': 'scatter3d',
                'mode': 'lines+markers',
                'line.shape': 'hvh',
                'name': `cross product`,
              },
            ],
          )}
          layout={{
            title: `Cross Product`,
          }}
        />

        <Plot
          data={(Object.keys(store.vectors) as Array<VectorName>).reduce(
            (vectors, vectorName) => [
              ...vectors,
              {
                'x': ['0', store.vectors[vectorName].x],
                'y': ['0', store.vectors[vectorName].y],
                'z': ['0', store.vectors[vectorName].z],
                'type': 'scatter3d',
                'mode': 'lines+markers',
                'line.shape': 'hvh',
                'name': `vector ${vectorName}`,
              },
            ],
            [
              {
                'x': ['0', add.x.toString()],
                'y': ['0', add.y.toString()],
                'z': ['0', add.z.toString()],
                'type': 'scatter3d',
                'mode': 'lines+markers',
                'line.shape': 'hvh',
                'name': `add`,
              },
            ],
          )}
          layout={{
            title: `Add`,
          }}
        />
        {!!scalarProduct && (
          <Plot
            data={[
              {
                'x': ['0', store.vectors.a.x],
                'y': ['0', store.vectors.a.y],
                'z': ['0', store.vectors.a.z],
                'type': 'scatter3d',
                'mode': 'lines+markers',
                'line.shape': 'hvh',
                'name': `vector a`,
              },
              {
                'x': ['0', scalarProduct.x.toString()],
                'y': ['0', scalarProduct.y.toString()],
                'z': ['0', scalarProduct.z.toString()],
                'type': 'scatter3d',
                'mode': 'lines+markers',
                'line.shape': 'hvh',
                'name': `scalarProduct`,
              },
            ]}
            layout={{
              title: `Scalar Product`,
            }}
          />
        )}
      </section>
    </section>
  )
}

export default App
