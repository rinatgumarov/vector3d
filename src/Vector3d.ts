class Vector3d {
  x: number = 0
  y: number = 0
  z: number = 0
  constructor({
    x,
    y,
    z,
  }: {
    x: string | number
    y: string | number
    z: string | number
  }) {
    if (!!x && !!y && !!z) {
      this.x = typeof x === 'number' ? x : parseFloat(x)
      this.y = typeof y === 'number' ? y : parseFloat(y)
      this.z = typeof z === 'number' ? z : parseFloat(z)
    }
  }

  add(vector: Vector3d) {
    return new Vector3d({
      x: this.x + vector.x,
      y: this.y + vector.y,
      z: this.z + vector.z,
    })
  }

  scalarProduct(multiplicator: number) {
    return !!multiplicator
      ? new Vector3d({
          x: this.x * multiplicator,
          y: this.y * multiplicator,
          z: this.z * multiplicator,
        })
      : undefined
  }

  crossProduct(vector: Vector3d) {
    return new Vector3d({
      x: this.y * vector.z - this.z * vector.y,
      y: this.z * vector.x - this.x * vector.z,
      z: this.x * vector.y - this.y * vector.x,
    })
  }

  dotProduct(vector: Vector3d) {
    return this.x * vector.x + this.y * vector.y + this.z * vector.z
  }

  scalarTripleProduct(a: Vector3d, b: Vector3d) {
    return this.dotProduct(a.crossProduct(b))
  }

  toString() {
    return `[${this.x}, ${this.y}, ${this.z}]`
  }
}

export default Vector3d
